public class Application{
	public static void main(String[] args){
		Student studies = new Student();
		studies.assignments = 6;
		studies.checkScore = 58;
		studies.course = "SQL";
		
		Student homework = new Student();
		homework.assignments = 3;
		homework.checkScore = 78;
		homework.course = "Calculus";
		
		Student[] section4 = new Student[3];
		section4[0] = studies;
		section4[1] = homework;
		section4[2] = homework;
		
		
		System.out.println(section4[2].course);
		
		/*homework.workload();
		studies.workload();
		
		homework.gradePass();
		studies.gradePass();*/
	}
}